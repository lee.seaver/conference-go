import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    # Create the URL for the request with the city and state
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    # Make the request
    response = requests.get(
        url, headers=headers, params=params
    )  # how is params beign inputted?

    # Parse the JSON response
    content = json.loads(response.content)
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}
    # to protect from potential changes from api dev that could break due to dict call not working


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f'{city}, {state}, {"US"}',
        "appid": OPEN_WEATHER_API_KEY,
    } 
    # Make the request
    response = requests.get(url, params=params)
    # Parse the JSON response
    content = json.loads(response.content)
   
    # Get the latitude and longitude from the response
    latitude = content[0]["lat"]
    longitude = content[0]["lon"]
    # Create the URL for the current weather API with the latitude
    #   and longitude
    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": latitude,
        "lon": longitude,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY
        }
    # Make the request
    response = requests.get(weather_url, params=params)
    # Parse the JSON response
    content = json.loads(response.content)
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
    return {
        "temp": content['main']['temp'],
        "description": content['weather'][0]['main']
    }
